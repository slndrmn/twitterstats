# -*- coding: utf-8 -*-

# from datetime import datetime
import re
import io
import sys
import csv

# Declare version
version = "0.2"

# Date regex
# date_regex = re.compile(r"[0-9]{4}-[0-9]{2}-[0-9]{2}")

# Check if -v is given
if len(sys.argv) > 1:
    if sys.argv[1] == "-v":
        print("TwitterStats " + version)
        exit()
else:
    print("[ERROR] Missing filepath. Please provide the path to the csv file as first argument.")
    exit(1)

# Open csv file or exit if file does not exist
try:
    csv_file = io.open(sys.argv[1], "r", encoding = "utf8")
except FileNotFoundError as error:
    print("[ERROR] Specified file doesn't exist.")
    exit(1)

tweets = []
output = "\n=====================================\n"
output += "            TWITTER STATS          \n"
output += "=====================================\n\n"

# Read csv file and save relevant tweet properties
# maximum: max number of tweets to be analysed; will be ignored if it is less than 1
# from_date: maximum date of tweet creation
# to_date: minimum date of tweet creation
maximum, from_date, to_date = None, None, None
if len(sys.argv) > 2:
    args_raw = sys.argv[2:]
    # Creates a "argument_name => argument_value"-like dictionary. All arguments behind the file path have to
    # be in a "key:value"-like format, e.g. "max:1000".
    args = dict()
    for arg in args_raw:
        args[str(arg).split(":")[0]] = str(arg).split(":")[1]

    if "max" in args:
        if args["max"] != ".":
            try:
                maximum = int(args["max"])
            except ValueError as error:
                maximum = -1

    if "from" in args:
        from_date = args["from"]

    if "to" in args:
        to_date = args["to"]

    # Swap from_date and to_date to avoid a negative timespan
    if from_date and to_date:
        if int(from_date) < int(to_date):
            temp = from_date
            from_date = to_date
            to_date = temp

if maximum:
    if maximum < 1:
        print('\n[WARNING] Invalid maximum, therefore it\'s being ignored. Please use a number greater than 1 or a simple "." (= no maximum).\n')
        maximum = None

steps = 0
for row in csv.reader(csv_file):
    if maximum and steps >= maximum:
        break

    # Ignore header line
    if row[0] == "tweet_id":
        continue

    # Rows look like this:
    # [0]   tweet_id
    # [1]   in_reply_to_status_id
    # [2]   in_reply_to_user_id
    # [3]   timestamp
    # [4]   source
    # [5]   text
    # [6]   retweeted_status_id
    # [7]   retweeted_status_user_id
    # [8]   retweeted_status_timestamp
    # [9]   expanded_urls

    tweet = dict()
    tweet["date_utc"] = str(row[3][:10]).replace("-", "").replace("-", "")
    tweet["time_utc"] = row[3][11:-6].replace(":", "")
    tweet["is_reply"] = row[1] != ""
    tweet["is_retweet"] = row[6] != ""
    tweet["source"] = row[4][row[4].index(">") + 1:-4]

    # Get mentions by using a regluar expression and remove leading "@" characters
    if not tweet["is_retweet"]:
        tweet["mentions"] = list(map(lambda x: x[1:], re.findall(r"@[a-zA-Z0-9_]{1,15}", row[5])))
    else:
        tweet["mentions"] = []

    add = False
    if from_date:
        if int(tweet["date_utc"]) <= int(from_date):
            add = True
    else:
        add = True

    if from_date and to_date:
        if (int(tweet["date_utc"]) <= int(from_date)) and (int(tweet["date_utc"]) >= int(to_date)):
            add = True
        else:
            add = False
    
    if add:
        print(str(steps + 1) + " - " + str(tweet))
        tweets.append(tweet)
        steps += 1

#
# This is where the cool stuff begins
#

clients = dict()
mentions = dict()
types = {
    "Puretweets": 0,
    "Replies": 0,
    "Retweets": 0,
    "Mentions": 0 # Not seen as distinct category
}

# Count clients
for tweet in tweets:
    if tweet["source"] in clients:
        clients[tweet["source"]] += 1
    else:
        clients[tweet["source"]] = 1

    # Count retweets, replies and pure tweets
    if tweet["is_retweet"]:
        types["Retweets"] += 1
    elif tweet["is_reply"]:
        types["Replies"] += 1
    else:
        types["Puretweets"] += 1

    # Count Twitter handles for mentions
    for mention in tweet["mentions"]:
        types["Mentions"] += 1
        if mention in mentions:
            mentions[mention] += 1
        else:
            mentions[mention] = 1

swapped_clients = dict()
swapped_mentions = dict()
ranked_clients = []
ranked_mentions = []

# Number of tweets analysed
output += "Tweets: " + str(len(tweets)) + "\n\n"

# Rank and print clients
# Quit if there are no tweets
if len(tweets) == 0:
    print("[WARNING] It seems like there are no tweets matching your criteria...")
    exit()

rank = 1
output += "Most used clients:\n"
for client in clients:
    swapped_clients[clients[client]] = client

swapped_clients_sorted = sorted(swapped_clients.keys(), reverse = True)
while len(swapped_clients_sorted) > 3:
        swapped_clients_sorted.pop()
for i in range(len(swapped_clients_sorted)):
    ranked_clients.append(swapped_clients[swapped_clients_sorted[i]])

for client in ranked_clients:
    output += str(rank) + ") " + client + ": " + str(round(int(clients[client]) * 100 / len(tweets), 1)) + "%\n"
    rank += 1

# Output types
output += "\nTweet types:\n"
for tweet_type in types:
    if tweet_type == "Mentions":
        continue
    output += "- " + tweet_type + ": " + str(round(int(types[tweet_type]) * 100 / len(tweets), 1)) + "%\n"

# Rank and output mentions
output += "\nMost mentions to:\n"
for mention in mentions:
    swapped_mentions[mentions[mention]] = mention

swapped_mentions_sorted = sorted(swapped_mentions.keys(), reverse = True)
while len(swapped_mentions_sorted) > 3:
        swapped_mentions_sorted.pop()
for i in range(len(swapped_mentions_sorted)):
    ranked_mentions.append(swapped_mentions[swapped_mentions_sorted[i]])

rank = 1
for mention in ranked_mentions:
    output += str(rank) + ") @" + u"\u200b" + mention + ": " + str(round(int(mentions[mention]) * 100 / types["Mentions"], 1)) + "%\n"
    rank += 1

# Output stats
print(output)