# TwitterStats

Ein Python-Skript zum Auswerten des eigenen Twitter-Archivs.

## Funktionsweise

Dem Twiter-Achiv liegt eine Liste aller Tweets im CSV-Format vor. Diese wird ausgelesen und die Tweets werden in Bezug auf
* Art (normaler Tweet, Antwort, Retweet)
* Client
* Mentions

analysiert. Am Ende erfolgt eine Ausgabe, welche die 3 meistgenutzten Clients (ggf. weniger), die 3 Personen, die am häufigsten erwähnt wurden (ggf. weniger) und den prozentualen Anteil der oben genannten Tweet-Arten enthält. Sie könnte bspw. wie folgt aussehen:

```
=====================================
            TWITTER STATS
=====================================

Tweets: 12119

Most used clients:
1) Twitter for Android: 53.2%
2) Twitter Lite: 21.3%
3) Twitter for iPhone: 17.2%

Tweet types:
- Puretweets: 38.2%
- Replies: 32.9%
- Retweets: 28.9%

Most mentions to:
1) @twitter_user: 14.5%
2) @notABot69924708: 11.7%
3) @johndoe: 11.4%
```

## Vorbereitungen

Um TweetStats nutzen zu können, müssen folgende Vorbereitungen getroffen werden:  
* Installation von mindestens Python 3.6 (sofern noch nicht vorhanden)
* [Anfordern](https://twitter.com/settings/account), Herunterladen und Entpacken des eigenen Twitter-Archivs

Nach diesen Vorkehrungen kann das Skript verwendet werden. Eine Installation externer Libraries ist __nicht__ notwendig.

## Setup

Benötigt wird die Datei `tweets.csv`, welche sich im Root-Verzeichnis des Twitter-Archivs befindet.

## Verwendung

### Syntaxbeschreibung

`python twstats.py (-v | <filepath> [max:<value>] [from:<date> [to:<date>]])`

`date = YYYYMMDD`

### Ausgabe der Version

Um die Version des Skripts auszugeben, kann der Parameter `-v` verwendet werden. Der Aufruf sieht dann also wie folgt aus:  
`python twstats.py -v`

### Auswerten des Archivs

Um das Archiv auszuwerten, muss als erstes Argument der Pfad zur `.csv`-Datei aus dem Twitter-Archiv (üblicherweise `tweets.csv`) übergeben werden. Optional können dieser Angabe weitere Argumente übergeben werden. Dabei wird eine `key:value`-Notation verwendet; die Reihenfolge dieser Paare spielt keine Rolle. Möglich sind dabei:

Name | Wert(e) | Bedeutung | Beispiele
--- | --- | --- | ---
max | Integer | die maximale Anzahl an Tweets (von neu zu alt) | 100, 1000, 10000
from | Datum | alle Tweets, die älter sind als (einschließlich) | 20131231, 20120101
to | Datum | alle Tweets, die jünger sind als (einschließlich) | 20131231, 20120101

`from` und `to` sind dabei absteigend zu betrachten, da das Archiv mit dem neuesten Tweet beginnt und mit dem ältesten endet. Sind alle Tweets des Dezember 2013 gesucht, sähen die Argumente also so aus:

`from:20131231 to:20131201`

Sollte das `from`-Datum jedoch kleiner sein als das `to`-Datum, werden diese vertauscht, sodass diese beiden Grenzen auch genau umgekehrt interpretiert werden können. Wichtig ist jedoch, dass beide Werte im Format `YYYYMMDD` vorliegen; der 28. Februar 2016 wäre also durch `20160228` darzustellen.

Beispiele:
* `python twstats.py "/home/stuff/archive/tweets.csv"` wertet das gesamte Archiv aus
* `python twstats.py "/home/stuff/archive/tweets.csv" max:1000` wertet lediglich die neuesten 1000 Tweets aus
* `python twstats.py "/home/stuff/archive/tweets.csv" max:10000 from:20163112` wertet die "neuesten" 10000 Tweets aus, die am 31.12.2016 __oder davor__ verfasst wurden
* `python twstats.py "/home/stuff/archive/tweets.csv" from:20180630 to:20180101` wertet alle Tweets aus, die zwischen dem 01.01.2018 und dem 30.06.2018 verfasst wurden

Sollte das Maximum nicht zulässig sein (kein numerischer Wert oder kleiner als 1), wird eine Warnung ausgegeben, das Skript jedoch dennoch ohne Maximum ausgeführt.

## Zukunft und bekannte Probleme

Geplant ist die Ergänzung um eine zeitliche Einteilung und Auswertung, um zu analysieren, zu welcher Tageszeit man die meisten Tweets verfasst (hat).

Bekannt: die Fehlerbehandlung für die `from`- und `to`-Argumente ist ausbaufähig.